# TheLife

## Description
Au d�marrage du programme, le jeu cr�� une entit� "TheLife" qui vit un certain temps et � partir d'une dur�e, a une chance de cr�� un enfant "TheLife" mais aussi un autre type d'enfant "TheDeath". Au bout d'un moment il meurt. 

Les entit�s "TheDeath" ont peu de vie et se nourissent de la vie des "TheLife".

Les entit�s ont des noms, les enfants prennent un nom al�atoire + un nombre apr�s leur nom qui correspond au nombre d'anc�tre qu'ils ont, par exemple "Lucas 4".

## Sp�cificit�s

- TheLife
  - Vie max entre 20 et 30 secondes
  - Peut cr�er un enfant "TheLife" � partir de 15 secondes avec 10% de chance par secondes
    - Donne un nom al�atoire + espace + le nombre d'anc�tre qu'il a
  - Peut cr�er un enfant "TheDeath" � partir de 15 secondes avec 3% de chance par secondes
    - Donne un nom al�atoire + espace + le nombre d'anc�tre qu'il a

- TheDeath
  - Vie max � 5 secondes
  - Sa vie max augmente de 0.2 secondes toutes les secondes pour chaque "TheLife" en vie
  - Pour chaque entit� TheDeath vivante, diminue de 0.2 secondes la vie max des "TheLife"

- � chaque mort et naissance, afficher:
  - La liste de tous les anc�tres avec leurs nom et une indication en X s'il est mort ou rien s'il est vivant s�par� par des "=>" pour finir sur le  nom de l'entit�
  - Indiquer si c'est un TheDeath ou TheLife avec un emoji ou quelque chose de simple reconaissable
  - Par exemple: "X Paul => Michel 2 => X Jean-Charles 3"

## Suppl�mentaire
- � la mort de toutes les entit�s ou au bout de 5 minutes, afficher une liste avec tous les anc�tres de chaque entit�s � la suite sous la forme voulue
- Par exemple: 
```
    Paul
        -> Michel 2
            -> Jean-Charles 3
            -> Jacout 3
                -> Papy 4
                    [...]
        -> Charlie 2
            [...]
        -> Thierry 2
            [...]
```